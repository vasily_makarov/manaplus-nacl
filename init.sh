#!/bin/sh

LOCAL_PATH=`dirname $0`
LOCAL_PATH=`cd $LOCAL_PATH && pwd`

export PATH=$PATH:${LOCAL_PATH}/depot_tools

cd "$LOCAL_PATH"

# Fetching depot_tools

if [ ! -d "depot_tools" ]; then
	git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
fi

# Fetching naclports using gclient

export PATH="$PATH":`pwd`/depot_tools

if [ ! -d "naclports" ]; then
	mkdir naclports
fi

if [ ! -d "naclports/src" ]; then
	cd naclports
	gclient config --name=src  https://chromium.googlesource.com/external/naclports.git
	gclient sync
	cd ..
	cp -r ports naclports/src
fi

if [ ! -d "nacl_sdk" ]; then
	echo "No NaCl SDK environment presented! Make sure you've created symbolic link to nacl_sdk folder."
	exit 1
fi
