#!/bin/bash

LOCAL_PATH=`dirname $0`
LOCAL_PATH=`cd $LOCAL_PATH && pwd`

PEPPER_VER=canary

export PATH=$PATH:${LOCAL_PATH}/depot_tools
export NACL_SDK_ROOT=${LOCAL_PATH}/nacl_sdk/pepper_${PEPPER_VER}
export NACL_LIBC=newlib

cd "$LOCAL_PATH"
cd naclports/src

export VERBOSE=1

CMD="$1"
if [[ -z "${CMD}" ]]; then
    export CMD="manaplus"
fi

bin/naclports -f uninstall "${CMD}"
# rm -rf out/build/${CMD}/*
rm -rf out/packages/${CMD}*

bin/naclports -f build "${CMD}"
if [ "$?" != 0 ]; then
    echo "build error"
    exit 1
fi

bin/naclports install "${CMD}" -v --verbose-build
if [ "$?" != 0 ]; then
    echo "install error"
    exit 1
fi

if [[ "${CMD}" == "manaplus" ]]; then
    cd ../..
    ./install.sh
fi
