#!/bin/bash

export PATH=$PATH:`pwd`/depot_tools

cd naclports

gclient config --name=src  https://chromium.googlesource.com/external/naclports.git
gclient sync

cd ..
cp -r ports naclports/src
