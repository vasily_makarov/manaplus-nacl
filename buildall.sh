#!/bin/sh

set -e

LOCAL_PATH=`dirname $0`
LOCAL_PATH=`cd $LOCAL_PATH && pwd`

cd "$LOCAL_PATH"

export VERBOSE=1
./scripts/portsmake.sh manaplus
./install.sh
