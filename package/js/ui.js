jQuery.fn.centerX = function(x) {

	var o = $(this);
	if (o == undefined)
		return;
	if (x == undefined) {
		return o.offset().left + o.outerWidth() / 2;
	}
	
	o.each(function() {
		$(this).offset({left: x - $(this).outerWidth()/2});
	});
};

jQuery.fn.centerY = function(y) {

	var o = $(this);
	if (o == undefined)
		return;
	if (y == undefined) {
		return o.offset().top + o.outerHeight() / 2;
	}
	
	o.each(function() {
		$(this).offset({top: y - $(this).outerHeight() / 2});
	});
};
