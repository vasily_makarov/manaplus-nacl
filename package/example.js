// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

function moduleDidLoad() {

}

function handleProgress(event) {
	var loadPercent = 0.0;

	var progress = $('#progress_nexe');
	var progressLabel = $('#progress_nexe_label');

	if (event.lengthComputable && event.total > 0) {
		loadPercent = event.loaded / event.total * 100.0;
	} else {

		progress.progressbar({
			value: false,
			change: function() {
				progressLabel.text( parseInt(progress.progressbar('value')) + "%" );
			},
			complete: function() {
				progress.remove();
				progressLabel.remove();
			}
		});

		$(window).resize(function() {
			progress.centerX($(window).width() / 2);
			progress.centerY($(window).height() - 70);
			progressLabel.centerY(progress.centerY());
			progressLabel.centerX(progress.centerX());
		});
		$(window).resize();

		loadPercent = 0.0;
	}

	progress.progressbar('value', loadPercent);
	progressLabel.centerX(progress.centerX());
}

function handleLoad(event) {
	$("#progress_container").hide();
	$("#nacl_module").focus();
}

function handleCrash(event) {
	if (common.naclModule.exitStatus == 0)
		window.close();
}

var settings = {};

function loadSettings(callback, args) {
	var keys = Object.keys(settings);
	chrome.storage.local.get(keys, function(data) {
		for (var i = 0; i < keys.length; ++i) {
			if (data[keys[i]] == undefined)
				data[keys[i]] = settings[keys[i]];
			else
				settings[keys[i]] = data[keys[i]];
		}
		callback(args);
	});
}

function saveSettings() {
	chrome.storage.local.set(settings);
}

function runNaCl(args) {
	var size = settings['storage_size'] * 1024 * 1024;

	var attrs = args[5];
	attrs['fs_pers'] = size;
	attrs['fs_http'] = '/';

	window.resize(settings['width'], settings['height']);

	if (settings['fullscreen'])
		chrome.app.window.current().fullscreen();

	navigator.webkitPersistentStorage.requestQuota(size,
		function(bytes) {
			common.updateStatus('Allocated ' + bytes + ' bytes of persistant storage.');
			common.attachDefaultListeners();
			common.createNaClModule(args[0], args[1], args[2], settings.width, settings.height, attrs);

			var listenerDiv = document.getElementById('listener');
			listenerDiv.addEventListener('progress', handleProgress, true);
			listenerDiv.addEventListener('load', handleLoad, true);
			listenerDiv.addEventListener('crash', handleCrash, true);
		},
		function(e) { alert('Failed to allocate space') }
	);

}

function domContentLoaded(name, tc, config, width, height, attributes) {
	settings['storage_size'] = 100;

	settings['width'] = parseInt(width);
	settings['height'] = parseInt(height);
	settings['fullscreen'] = false;

	loadSettings(runNaCl, arguments);

	$(window).resize(function() {
		settings['width'] = $(window).width();
		settings['height'] = $(window).height();
		saveSettings();

		document.getElementById('nacl_module').setAttribute('width', Math.max($(window).width(), 500));
		document.getElementById('nacl_module').setAttribute('height', Math.max($(window).height(), 500));
	});
}

chrome.app.window.current().onFullscreened.addListener(function() {
	settings['fullscreen'] = true;
	saveSettings();
});

chrome.app.window.current().onRestored.addListener(function() {
	settings['fullscreen'] = false;
	saveSettings();
});
