// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

function onLaunched(launchData) {
  chrome.app.window.create('index.html', {
    width: 900,
    height: 700
  });
}

chrome.app.runtime.onLaunched.addListener(onLaunched);
