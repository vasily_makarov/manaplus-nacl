#!/bin/sh

LOCAL_PATH=`dirname $0`
LOCAL_PATH=`cd $LOCAL_PATH && pwd`

PEPPER_VER=canary

export PATH=$PATH:${LOCAL_PATH}/depot_tools
export NACL_SDK_ROOT=${LOCAL_PATH}/nacl_sdk/pepper_${PEPPER_VER}
export NACL_LIBC=newlib

cd "$LOCAL_PATH"
cd naclports/src

export VERBOSE=1

bin/naclports -f uninstall manaplus
rm -rf out/build/manaplus/*
rm -rf out/packages/manaplus*
