#!/bin/sh

set -e

LOCAL_PATH=`dirname $0`
LOCAL_PATH=`cd $LOCAL_PATH && pwd`

cd "$LOCAL_PATH"

rm -f "${LOCAL_PATH}/package/pnacl/Release/manaplus.bc"
cp "${LOCAL_PATH}/nacl_sdk/pepper_canary/toolchain/linux_pnacl/le32-nacl/usr/bin/manaplus.pexe" "${LOCAL_PATH}/package/pnacl/Release/manaplus.bc"

cd "${LOCAL_PATH}/package/pnacl/Release"
rm -f manaplus.pexe
"${LOCAL_PATH}/nacl_sdk/pepper_canary/toolchain/linux_pnacl/bin/pnacl-finalize" manaplus.bc -o manaplus.pexe
rm -f manaplus.bc

cd "${LOCAL_PATH}"
./scripts/zipit.sh
