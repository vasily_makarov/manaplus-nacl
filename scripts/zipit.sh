#!/bin/bash

DIR=`pwd`

rm package/data.zip
rm -rf package/data

if [ ! -d "naclports/src/out/build/manaplus/install_pnacl/payload/share/manaplus/" ]; then
    echo "manaplus directory not exists. Cant zip it."
    exit
fi

cd naclports/src/out/build/manaplus/install_pnacl/payload/share/manaplus/
mkdir -p "data/translations/manaplus"
cp -r $DIR/naclports/src/ports/manaplus/src/po/*.po "data/translations/manaplus"

cp -r data "$DIR/package/data"
#mkdir "$DIR/package/data/translations/manaplus"
#cp -r $DIR/naclports/src/ports/manaplus/src/po/*.po "$DIR/package/data/translations/manaplus"

rm -rf .data
cp -r data .data
cd .data

for f in fonts/*; do
	echo "Dummying $f..."
	rm -rf "$f"
	echo '' > "$f"
done

zip -r -Z store -0 "$DIR/package/data.zip" *

cd "$DIR"
rm manaplus.zip
zip -r manaplus.zip package
