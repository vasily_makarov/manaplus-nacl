#!/bin/sh

LOCAL_PATH=`dirname $0`
LOCAL_PATH=`cd $LOCAL_PATH/.. && pwd`

#PEPPER_VER=36
PEPPER_VER=canary
NACLPORTS_MAKE=make

export PATH=$PATH:${LOCAL_PATH}/depot_tools
export NACL_SDK_ROOT=${LOCAL_PATH}/nacl_sdk/pepper_${PEPPER_VER}
export NACL_LIBC=newlib

cd "$LOCAL_PATH"

if [ ! -d "naclports" ]; then
	echo "No NaCl ports environment presented! Make sure you've run init.sh."
	exit 1
fi

if [ ! -d "nacl_sdk" ]; then
	echo "No NaCl SDK environment presented! Make sure you've created symbolic link to nacl_sdk folder."
	exit 1
fi

# Making libraries

cd naclports/src
$NACLPORTS_MAKE $@

